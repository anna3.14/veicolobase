package org.example;

//TIP To <b>Run</b> code, press <shortcut actionId="Run"/> or
// click the <icon src="AllIcons.Actions.Execute"/> icon in the gutter.
public class Main {
    public static void main(String[] args) throws Exception {
        VeicoloBase veicolo=new VeicoloBase("Panda",70,10000,3);
        Accessorio accessorio1=new Accessorio("Vernice metallizzata",1000);
        Accessorio accessorio2=new Accessorio("Sensori",1200);

        veicolo.aggiungiAccessorio(accessorio1);
        veicolo.aggiungiAccessorio(accessorio2);

        Accessorio[] elenco=veicolo.ottieniAccessori();
        for(int i=0;i<elenco.length;i++){
            System.out.println(elenco[i].getNome());
        }

        System.out.println("--------------------------------------");
        veicolo.stampaAccessori();

        System.out.println("--------------------------------------");
        double consumo=veicolo.simulaViaggio(100,5.0/100);
        System.out.println("Consumo: "+consumo);

        System.out.println("--------------------------------------");
        System.out.println("La velocità media vale: "+veicolo.calcolaVelocitaMedia());


    }
}