package org.example;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;


/**
 * La classe VeicoloBase è la principale del programma. Da essa è possibile aggiungere accessori ai
 * veicoli, conoscere i consumi medi in un viaggio, determinare velocità media e massima del veicolo.
 */
public class VeicoloBase {
    private String tipoVeicolo;
    private int potenzaMotore;
    private List<Accessorio> accessori;
    private int numeroAccessori;
    private int numeroMaxAccessori;
    private double prezzo;

    /**
     * Costruttore con i parametri della classe VeicoloBase
     * @param tipoVeicolo
     * @param potenzaMotore
     * @param prezzo
     * @param numeroMaxAccessori
     */
    //Costruttore
    public VeicoloBase(String tipoVeicolo, int potenzaMotore, double prezzo,int numeroMaxAccessori){
            this.tipoVeicolo=tipoVeicolo;
            this.potenzaMotore=potenzaMotore;
            this.prezzo=prezzo;
            this.numeroAccessori=0;
            this.numeroMaxAccessori=numeroMaxAccessori;
            List<Accessorio> accessori=new ArrayList<>();
            this.accessori=accessori;
    }

    /**
     * Metodo per ottenere il tipo di veicolo
     * @return String
     */
    public String getTipoVeicolo() {
        return tipoVeicolo;
    }

    /**
     * Metodo per settare il tipo di veicolo
     * @param tipoVeicolo
     */
    public void setTipoVeicolo(String tipoVeicolo) {
        this.tipoVeicolo = tipoVeicolo;
    }

    /**
     * Metodo per ottenere la potenza del motore (espressa in cavalli(CV))
     * @return int
     */
    public int getPotenzaMotore() {
        return potenzaMotore;
    }

    /**
     * Metodo per settare la potenza del motore in cavalli(CV)
     * @param potenzaMotore
     */
    public void setPotenzaMotore(int potenzaMotore) {
        this.potenzaMotore = potenzaMotore;
    }

    /**
     * Metodo per ottenere il numero di accessori sul veicolo
     * @return int
     */
    public int getNumeroAccessori() {
        return numeroAccessori;
    }

    /**
     * Metodo per settare il numero di accessori nel veicolo
     * @param numeroAccessori
     */
    public void setNumeroAccessori(int numeroAccessori) {
        this.numeroAccessori = numeroAccessori;
    }

    /**
     * Metodo per ottenere il numero massimo di accessori
     * @return int
     */
    public int getNumeroMaxAccessori() {
        return numeroAccessori;
    }

    /**
     * Metodo per settare il numero massimo di accessori
     * @param numeroMaxAccessori
     */
    public void setNumeroMaxAccessori(int numeroMaxAccessori) {
        this.numeroAccessori = numeroAccessori;
    }

    /**
     * Metodo per ottenere il prezzo del veicolo
     * @return double
     */
    public double getPrezzo() {
        return prezzo;
    }

    /**
     * Metodo per settare il prezzo del veicolo
     * @param prezzo
     */
    public void setPrezzo(double prezzo) {
        this.prezzo = prezzo;
    }

    /**
     * Metodo per ottenere la lista di accessori
     * @return List<Accessorio>
     */
    public List<Accessorio> getAccessori() {
        return accessori;
    }

    /**
     * Metodo per settare la lista di accessori
     * @param accessori
     */
    public void setAccessori(List<Accessorio> accessori) {
        this.accessori = accessori;
    }

    /**
     * Metodo per aggiungere un accessorio al veicolo. Se si è superato il limite massimo di accessori
     * restituisce un eccezione con messaggio personalizzato.
     * @param accessorio
     * @throws Exception
     */
    //Metodo1: aggiungiAccessorio(Accessorio accessorio): aggiunge un accessorio al veicolo.
    public void aggiungiAccessorio(Accessorio accessorio) throws Exception {
        if ((accessori.size())+1>numeroMaxAccessori){
            throw new Exception("Raggiunto il limite massimo di accessori");
        }
        accessori.add(accessorio);
        System.out.println("Nuovo accessorio aggiunto");
    }

    /**
     * Metodo che restituisce un array copia dell'ArrayList accessori
     * @return Accessorio[]
     */
    //Metodo2:ottieniAccessori(): Accessorio[]: restituisce una copia dell'array degli accessori.
    public Accessorio[] ottieniAccessori(){
        int dimensione=accessori.size();
        Accessorio[] copiaAccessori=new Accessorio[dimensione];
        for(int i=0;i<accessori.size();i++){
            Accessorio accessorio=accessori.get(i);
            copiaAccessori[i]=accessorio;
        }
        return copiaAccessori;
    }

    //Metodo3:calcolaVelocitaMassima(): calcola e restituisce la
    // velocità massima del veicolo, tale calcolo deve essere eseguito con una
    // formula arbitraria sulla base della potenza del motore.

    /**
     * Metodo per calcolare sulla base di una formula arbitraria la velocità massima del veicolo.
     * @return double
     */
    public double calcolaVelocitaMassima(){
        double k=1.93; //Data da cose tipo Area Frontale del veicolo e attrito aria
        double vel=potenzaMotore*k;
        return vel;
    }

    /**
     * Metodo per stampare gli accessori con nome e prezzo su un certo veicolo
     */
    //Metodo4:stampaAccessori(): stampa la lista degli accessori nel veicolo.
    public void stampaAccessori(){
        for(Accessorio acc : accessori ){
            System.out.println(acc.getNome()+" "+acc.getPrezzo()+"€");
        }
    }

    //Metodo5:calcolaCostoTotale(): double: calcola e restituisce il
    // costo totale del veicolo, inclusi gli accessori.

    /**
     * Metodo per calcolare il costo totale del veicolo con accessori
     * @return double
     */
    public double calcolaCostoTotale(){
        double costo=0;
        for(Accessorio acc : accessori ){
            costo=costo+acc.getPrezzo();
        }
        double costoTotale=costo+prezzo;
        return costoTotale;

    }

    /**
     * Metodo che restituisce il consumo a seconda della distanza prevista e del consumo medio(espresso in l/100km)
     * @param distanza
     * @param consumoMedio
     * @return double
     */
    //Metodo6:simulaViaggio(double distanza, double consumoMedio):
    // simula il consumo in litri di un viaggio considerando distanza e consumo medio (es in 5l/100km).
    public double simulaViaggio(double distanza, double consumoMedio){
        double consumo=consumoMedio*distanza;
        return consumo;

    }

    /**
     * Metodo che ritorna la velocità media del veicolo, estrandola casualmente tra 0 e 150 km/h
     * @return double
     */
    //Metodo7:calcolaVelocitaMedia(): calcola randomicamente un numero che va
    // da 1 a 100 e restituisce una velocità media uguale an numero randomico.
    public double calcolaVelocitaMedia(){
        Random random=new Random();
        double media=random.nextDouble(0,150);
        return media;
    }


}
