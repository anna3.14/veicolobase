package org.example;

/**
 * Classe Accessorio con i metodi getter/setter e costruttore con parametri.
 */
public class Accessorio {
    private String nome;
    private double prezzo;

    /**
     * Costruttore della classe Accessorio
     * @param nome
     * @param prezzo
     */
    //Costruttore
    public Accessorio(String nome, double prezzo){
        this.nome=nome;
        this.prezzo=prezzo;
    }

    /**
     * Metodo per ottenere il nome dell'accessorio
     * @return String
     */
    public String getNome() {
        return nome;
    }

    /**
     * Metodo per settare il nome dell'accessorio
     * @param nome
     */
    public void setNome(String nome) {
        this.nome = nome;
    }

    /**
     * Metodo per ottenere il prezzo dell'accessorio
     * @return double
     */
    public double getPrezzo() {
        return prezzo;
    }

    /**
     * Metodo per settare il prezzo dell'accessorio
     * @param prezzo
     */
    public void setPrezzo(double prezzo) {
        this.prezzo = prezzo;
    }
}
