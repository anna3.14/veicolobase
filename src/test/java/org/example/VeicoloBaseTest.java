package org.example;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Metodi di test per verificare la corretta creazione degli oggetti, il calcolo della velocità massima
 * di un veicolo e il calcolo del costo totale del veicolo stesso.
 */
class VeicoloBaseTest {
    private VeicoloBase veicolo;
    private Accessorio accessorio1;
    private Accessorio accessorio2;
    private Accessorio accessorio3;

    /**
     * Vengono istanziati e inizializzati tutti gli oggetti usati per il test. Questo test, in modalità
     * Beforeeach, verrà ripetuto prima di ogni test successivo.
     */
    @BeforeEach
     void setUp(){
         veicolo=new VeicoloBase("Panda",70, 10235,1);
         accessorio1=new Accessorio("stereo",453);
         accessorio2=new Accessorio("sensori",1013);
         accessorio3=new Accessorio("colore metallizzato",900);
    }

    /**
     * Metodo di test per controllare che gli oggetti non siano nulli e per testare i vari metodi getter e setter
     */
    @Test //Controllo che tutti gli oggetti istanziati non siano nulli e che funzionano i getter e setter
    void testIstanze(){
        assertNotNull(veicolo);
        assertNotNull(accessorio1);
        assertNotNull(accessorio2);
        assertNotNull(accessorio3);
        //Test get e set di VeicoloBase
        assertEquals("Panda",veicolo.getTipoVeicolo());
        assertEquals(70,veicolo.getPotenzaMotore());
        assertEquals(10235,veicolo.getPrezzo());

        //Test get e set di Accessorio
        assertEquals("stereo", accessorio1.getNome());
        assertEquals(453, accessorio1.getPrezzo());

    }

    /**
     * Metodo di test per controllare la correttezza del conto della velocità massima
     */
    @Test //Controllo il calcolo della velocità massima
    void testVelocitaMassima(){
        assertEquals(135.1,veicolo.calcolaVelocitaMassima());
    }

    /**
     * Metodo di test per verificare la correttezza della stima del costo del veicolo con o senza accessori
     */
    @Test //In caso di accessori sotto il limite max
    void testCalcolaCostoTotale(){
        try {
            veicolo.aggiungiAccessorio(accessorio1);
            veicolo.aggiungiAccessorio(accessorio2);
            veicolo.aggiungiAccessorio(accessorio3);
            assertEquals(12601.0,veicolo.calcolaCostoTotale());

        }catch(Exception e){
            System.out.println("Raggiunto numero limite accessori");
        }

    }


}